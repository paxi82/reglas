Normas y Pautas
======================

**Contenido:**

 - [Normas Generales de la comunidad](https://gitlab.com/programadores_chile/reglas/tree/master/normas_generales.md)
 - [Pautas oficiales de la comunidad](https://gitlab.com/programadores_chile/reglas/tree/master/pautas_oficiales.md)

**Todo miembro de la comunidad puede enviar sus sugerencias, ya sea, para mejorar las normas y pautas, como para proponer proyectos futuros.**

**Derechos de autor**

- Los iconos, ilustraciones y cualquier otra imágen utilizada, ha sido creada en base a los siguientes recursos:
[Flaticon](https://www.flaticon.com)
[Canva](https://www.canva.com)